# SpaceDIM

Project develloped for a school class, see screenshots bellow :

## Choose a username
![image alt](https://i.ibb.co/QXgY1gX/1.png "Choose username")

## Choose a waiting room
![image alt](https://i.ibb.co/McYf4NJ/2.png "Choose a waiting room")

## Waiting for the others players
![image alt](https://i.ibb.co/QcYNbz9/3.png "Waiting for the others players")

## Playing the game
![image alt](https://i.ibb.co/Lt6pnXq/4.png "Playing the game")

## Win screen
![image alt](https://i.ibb.co/64TgNgZ/5.png "Win screen")

## Loose screen
![image alt](https://i.ibb.co/dL4qSMz/6.png "Loose screen")

## Leaderboard
![image alt](https://i.ibb.co/vZdyVdY/7.png "Leaderboard")


