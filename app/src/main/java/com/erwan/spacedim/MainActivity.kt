package com.erwan.spacedim

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.erwan.spacedim.API.SocketListener
import com.erwan.spacedim.views.login.LoginViewModel
import okhttp3.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    var loginViewModelTraveler = LoginViewModel();

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val client = OkHttpClient.Builder()
            .readTimeout(3, TimeUnit.SECONDS)

            .build()
        val request = Request.Builder()
            .url("ws://spacedim.async-agency.com:8081")
            .build()
        val socketListener = SocketListener()
        val webSocket = client.newWebSocket(request, socketListener)

        client.dispatcher.executorService.shutdown()
    }

    override fun onBackPressed() {
    }
}