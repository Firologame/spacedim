package com.erwan.spacedim.API

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserPost(val name: String)