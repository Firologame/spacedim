package com.erwan.spacedim.views.game

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.erwan.spacedim.API.MyWebsocketTraveler
import com.erwan.spacedim.API.SocketListener
import com.erwan.spacedim.API.User


class GameViewModel(
    player: User,
    webSocketTraveler: MyWebsocketTraveler
) : ViewModel() {

    var currentWebSocket = webSocketTraveler.getWebsocket()
    var myUser = player
    var currentListener = webSocketTraveler.getlistener()
    val gameState: MutableLiveData<SocketListener.Event> = currentListener.gameState
    val gameUi: MutableLiveData<List<SocketListener.UIElement>> = currentListener.gameUi
    var myPlayer = player
}
