package com.erwan.spacedim.views.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erwan.spacedim.API.MyWebsocketTraveler
import com.erwan.spacedim.API.User

class GameViewModelFactory(private val player: User, private val webSocketTraveler: MyWebsocketTraveler) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameViewModel::class.java)) {
            return GameViewModel(player,webSocketTraveler) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}