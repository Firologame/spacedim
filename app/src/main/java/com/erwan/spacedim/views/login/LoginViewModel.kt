package com.erwan.spacedim.views.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erwan.spacedim.API.*
import kotlinx.coroutines.launch
import okhttp3.WebSocket

class LoginViewModel : ViewModel() {
    private val TAG = "LoginViewModel"

    private val _response = MutableLiveData<String>()

    val response: LiveData<String>
        get() = _response

    private var _userFromAPI = MutableLiveData<User>()
    val userFromAPI: LiveData<User>
        get() = _userFromAPI

    val listener = SocketListener()
    var webSocket: WebSocket? = null


    init {
        Log.i(TAG, "ViewModel Linked")
    }

    fun findUser(userName: String?) {
        var user: User
        viewModelScope.launch {
            try {
                val userFromAPI = SpaceDimApi.retrofitService.findUser(userName.toString())
                val userName = userFromAPI.name
                val userId = userFromAPI.id
                val userAvatar = userFromAPI.avatar
                val userScore = userFromAPI.score
                _userFromAPI.value = User(userId, userName, userAvatar, userScore, State.OVER)
                logUser(userId)
            } catch (e: Exception) {
                println(e)
                createUser(userName.toString())
            }
        }
    }



    fun logUser(userId: Int) {
        viewModelScope.launch {
            try {
                val user = SpaceDimApi.retrofitService.logUser(userId)
            } catch (e: Exception) {
                Log.i(TAG, e.message.toString())
            }
        }
    }

    fun createUser(userName: String) {
        viewModelScope.launch {
            try {
                val newUser = UserPost(userName)
                val service = SpaceDimApi.retrofitService.createUser(newUser)

            } catch (e: Exception) {
                Log.i(TAG, e.message.toString())
            }
        }
    }

}
