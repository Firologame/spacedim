package com.erwan.spacedim.views.game

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.erwan.spacedim.API.PolymorphicAdapter
import com.erwan.spacedim.API.SocketListener
import com.erwan.spacedim.R
import com.erwan.spacedim.databinding.GameFragmentBinding
import kotlinx.android.synthetic.main.tablerow.*

class GameFragment : Fragment() {

    private lateinit var binding: GameFragmentBinding
    private lateinit var viewModelFactory: GameViewModelFactory
    private lateinit var viewModel: GameViewModel

    //les observers
    private lateinit var gameStateObserver: Observer<SocketListener.Event>
    private lateinit var gameUiObserver: Observer<List<SocketListener.UIElement>>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =  GameFragmentBinding.inflate(inflater,container,false)

        viewModelFactory = GameViewModelFactory(
            GameFragmentArgs.fromBundle(requireArguments()).user,
            GameFragmentArgs.fromBundle(requireArguments()).webSocket
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)

        // create a variable that have a range and can be updated later for the progress bar (and create a listener bellow)
        val valueAnimator = ValueAnimator.ofInt(0, 255)
        valueAnimator.addUpdateListener {
            binding.progressBar.progress = (valueAnimator.animatedValue as Int)
        }

        // create an observer that listen for the game state (win, loose, next level)
        gameStateObserver = Observer<SocketListener.Event> { newState ->
            if (newState.type == SocketListener.EventType.GAME_OVER) {
                var gameover_action = newState as SocketListener.Event.GameOver
                if (gameover_action.win) {
                    nextScreenWin(gameover_action.score)
                } else {
                    nextScreenLose(gameover_action.score)
                }
            }

            if (newState.type == SocketListener.EventType.NEXT_LEVEL) {
                var gamenextlevel_action = newState as SocketListener.Event.NextLevel

                buildButtons(gamenextlevel_action.uiElementList)
            }
            if (newState.type == SocketListener.EventType.NEXT_ACTION) {
                var gamenextaction_action = newState as SocketListener.Event.NextAction
                valueAnimator.end()
                valueAnimator.duration = gamenextaction_action.action.time
                valueAnimator.start()
                sendaction(gamenextaction_action.action)
            }
        }
        viewModel.gameState.observe(viewLifecycleOwner, gameStateObserver)


        gameUiObserver = Observer<List<SocketListener.UIElement>> { elements ->
            buildButtons(elements)

        }
        viewModel.gameUi.observe(viewLifecycleOwner, gameUiObserver)

        return binding.root
    }

    // function that navigate to loose fragment
    private fun nextScreenLose(scoreFinal: Int) {
        viewModel.gameState.removeObserver(gameStateObserver)
        val action = GameFragmentDirections.actionGameDestinationToLooseDestination(viewModel.myPlayer, scoreFinal)
        viewModel.currentWebSocket.close(1000,"Game ends");
        NavHostFragment.findNavController(this).navigate(action)
    }

    // function that navigate to win fragment
    private fun nextScreenWin(scoreFinal: Int) {
        viewModel.gameState.removeObserver(gameStateObserver)
        val action = GameFragmentDirections.actionGameDestinationToWinDestination(viewModel.myPlayer, scoreFinal)
        viewModel.currentWebSocket.close(1000,"Game ends");
        NavHostFragment.findNavController(this).navigate(action)
    }

    // function that build game button actions
    private fun buildButtons(elements: List<SocketListener.UIElement>) {
        var itemperrow = 2
        var count = 0
        binding.layoutTable.removeAllViews()


        var currenttablerow = createTablerow()
        for (element in elements) {
            if (element.type == SocketListener.UIType.SWITCH)
                createSwitch(currenttablerow, element)
            else if (element.type == SocketListener.UIType.BUTTON)
                createButton(currenttablerow, element)

            count++
            if (count >= itemperrow) {
                count = 0;
                binding.layoutTable.addView(currenttablerow)
                currenttablerow = createTablerow()
            }
        }
    }

    // function that build the container when is displayed game buttons / switchs
    private fun createTablerow(): TableRow {
        val inflater = LayoutInflater.from(this.context)
        val table = inflater.inflate( R.layout.tablerow,view_table,false) as TableRow
        return table
    }

    // function that create game button
    private fun createButton(row: TableRow, element: SocketListener.UIElement) {
        val inflater = LayoutInflater.from(this.context)
        val button = inflater.inflate(
            R.layout.button_only,
            row,
            false
        )
        button.findViewById<Button>(R.id.button).setOnClickListener {
            sendelementclick(element)
        }
        val content = element.content.toLowerCase()
        button.findViewById<Button>(R.id.button).text = content
        row.addView(button)

    }

    // function that create game switch
    private fun createSwitch(
        row: TableRow,
        element: SocketListener.UIElement
    ) {
        val inflater = LayoutInflater.from(this.context)
        val switch = inflater.inflate(
            R.layout.switch_only,
            row,
            false)
        val content = element.content
        switch.findViewById<TextView>(R.id.text_switch).text = content
        switch.findViewById<Switch>(R.id.switch1).setOnClickListener {
            sendelementclick(element)
        }
        row.addView(switch)
    }

    // function that send the data when a button is clicker or a switch state changed
    private fun sendelementclick(element: SocketListener.UIElement) {
        viewModel.currentWebSocket.send(
            PolymorphicAdapter.eventGameParser.toJson(
                SocketListener.Event.PlayerAction(
                    element
                )
            )
        )
    }

    private fun sendaction(action: SocketListener.Action) {
        binding.textAction.text = action.sentence
    }
}