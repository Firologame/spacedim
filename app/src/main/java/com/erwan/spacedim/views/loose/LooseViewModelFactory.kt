package com.erwan.spacedim.views.loose

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erwan.spacedim.API.User

class LooseViewModelFactory(private val player: User, private val scoreFinal: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LooseViewModel::class.java)) {
            return LooseViewModel(player, scoreFinal) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}