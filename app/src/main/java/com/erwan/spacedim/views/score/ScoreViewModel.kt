package com.erwan.spacedim.views.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erwan.spacedim.API.SpaceDimApi
import com.erwan.spacedim.API.State
import com.erwan.spacedim.API.User
import kotlinx.coroutines.launch

class ScoreViewModel(player : User) : ViewModel() {

    val currentPlayer = player
    private val TAG = "ScoreViewModel"

    private val _userScoreList = MutableLiveData<List<User>>()
    init {
        listAllUser()
    }

    val userScoreList: LiveData<List<User>>
        get() = _userScoreList

    fun listAllUser() {
        viewModelScope.launch {
            try {
                val listUser = mutableListOf<User>()
                val users = SpaceDimApi.retrofitService.listAllUser()
                for (user in users){
                    if (listUser.size > 10)
                        break
                    val userName = user.name
                    val userId = user.id
                    val userAvatar = user.avatar
                    val userScore = user.score
                    listUser += User(userId, userName, userAvatar, userScore, State.OVER)
                    Log.i(TAG, userName)
                }
                _userScoreList.postValue(listUser)
            } catch (e: Exception) {
                println(e)
            }
        }
        Log.i(TAG, _userScoreList.toString())
    }
}