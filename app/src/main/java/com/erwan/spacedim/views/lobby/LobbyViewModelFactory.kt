package com.erwan.spacedim.views.lobby

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erwan.spacedim.API.User

class LobbyViewModelFactory(private val player: User) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LobbyViewModel::class.java)) {
            return LobbyViewModel(player) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}