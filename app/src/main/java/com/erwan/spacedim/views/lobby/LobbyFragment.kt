package com.erwan.spacedim.views.lobby

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.erwan.spacedim.API.MyWebsocketTraveler
import com.erwan.spacedim.R
import com.erwan.spacedim.API.SocketListener
import com.erwan.spacedim.API.State
import com.erwan.spacedim.API.User
import com.erwan.spacedim.databinding.LobbyFragmentBinding


class LobbyFragment : Fragment() {

    private lateinit var viewModel: LobbyViewModel
    private lateinit var viewModelFactory: LobbyViewModelFactory

    private lateinit var binding: LobbyFragmentBinding
    private var ready = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = LobbyFragmentBinding.inflate(inflater,container,false)
        binding.buttonJoinRoom.setOnClickListener {
            var roomName = binding.inputRoomName.text.toString()
            if(roomName.length !== 0) {
                viewModel.joinRoom(roomName,viewModel.currentPlayer)

                binding.imageBackgroundJoin.visibility = View.INVISIBLE
                binding.inputRoomName.visibility = View.INVISIBLE
                binding.buttonJoinRoom.visibility = View.INVISIBLE
                binding.textRoomName.text = getString(R.string.text_room) + " : " + roomName
                binding.imageListPlayers.visibility = View.VISIBLE
                binding.layoutConnected.visibility = View.VISIBLE

                hideKeyboard()

                binding.buttonReady.setOnClickListener {

                    // when click on ready button, send ready on server, or send not ready (change text button)
                    if(!ready) {
                        viewModel.sendready()
                        ready = true
                        binding.buttonReady.text = getString(R.string.button_not_ready)
                    }
                    else {
                        viewModel.sendnotready()
                        ready = false
                        binding.buttonReady.text = getString(R.string.button_ready)
                    }
                }
            }
        }

        viewModelFactory = LobbyViewModelFactory(LobbyFragmentArgs.fromBundle(requireArguments()).user)
        viewModel = ViewModelProvider(this, viewModelFactory).get(LobbyViewModel::class.java)


        val gameStarterObserver = Observer<SocketListener.Event> { newState ->
            if (newState.type == SocketListener.EventType.WAITING_FOR_PLAYER) {
                var lobbywaitforplayer_action = newState as SocketListener.Event.WaitingForPlayer
                drawUsers(lobbywaitforplayer_action.userList)
                if (lobbywaitforplayer_action.userList.size > 1) {
                    var alluserrdy = true
                    for (user in lobbywaitforplayer_action.userList) {
                        if (user.state == State.WAITING) {
                            alluserrdy = false
                        }
                    }

                    // if all users ready, navigate to game fragment
                    if (alluserrdy) {
                        val action =
                            LobbyFragmentDirections.actionLobbyDestinationToGameDestination(
                                viewModel.currentPlayer,
                                MyWebsocketTraveler(viewModel.webSocket!!, viewModel.listener)
                            )
                        NavHostFragment.findNavController(this).navigate(action)

                    }
                }

            }
        }
        viewModel.gameState.observe(viewLifecycleOwner, gameStarterObserver)

        return binding.root
    }

    private fun createPlayerContainer(user: User) : ConstraintLayout {
        val inflater =LayoutInflater.from(this.context)
        val playertile = inflater.inflate(R.layout.user_container,null,false) as ConstraintLayout
        val playerimg_ready = playertile.findViewById<ImageView>(R.id.image_ready)
        val playerimg_not_ready = playertile.findViewById<ImageView>(R.id.image_not_ready)
        val name = playertile.findViewById<TextView>(R.id.text_player_name)
        val score = playertile.findViewById<TextView>(R.id.text_score)

        // if user is waiting, hide ready image and display waiting one
        if (user.state == State.WAITING) {
            playerimg_not_ready.visibility = View.VISIBLE
            playerimg_ready.visibility = View.INVISIBLE
        }

        // if user is ready, show ready image and hide waiting one
        else if(user.state == State.READY) {
            playerimg_not_ready.visibility = View.INVISIBLE
            playerimg_ready.visibility = View.VISIBLE
        }

        name.text = user.name
        score.text = user.score.toString()
        return playertile
    }

    // Create a player container and add it in playerList
    private fun drawUsers(users : List<User>){
        binding.playerList.removeAllViews()
        for (user in users){
            binding.playerList.addView(createPlayerContainer(user))
        }
    }
}


// Used to hide keyboard when in fragment, activity or context
fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}