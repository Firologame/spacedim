package com.erwan.spacedim.views.win

import androidx.lifecycle.ViewModel
import com.erwan.spacedim.API.User

class WinViewModel(player : User, scoreFinal: Int) : ViewModel() {

    var myPlayer = player
    var scoreFinal = scoreFinal
}