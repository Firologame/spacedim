
package com.erwan.spacedim.views.lobby

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.erwan.spacedim.API.SocketListener
import com.erwan.spacedim.API.User
import okhttp3.*

class LobbyViewModel(player : User) : ViewModel() {

    val listener = SocketListener()
    var webSocket: WebSocket? = null
    val gameState:MutableLiveData<SocketListener.Event> = listener.gameState
    val currentPlayer = player

    fun joinRoom(roomName: String, user: User){
        val client = OkHttpClient()
        val request = Request.Builder().url("ws://spacedim.async-agency.com:8081/ws/join/" + roomName + "/" + user.id.toString()).build();
        webSocket = client.newWebSocket(request, listener)
    }

    fun sendready(){
        webSocket?.send("{\"type\":\"READY\", \"value\":true}");
    }
    fun sendnotready(){
        webSocket?.send("{\"type\":\"READY\", \"value\":false}");
    }

}
