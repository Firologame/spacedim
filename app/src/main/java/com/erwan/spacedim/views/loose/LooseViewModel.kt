package com.erwan.spacedim.views.loose

import androidx.lifecycle.ViewModel
import com.erwan.spacedim.API.User

class LooseViewModel(player: User, scoreFinal: Int) : ViewModel() {
    var myPlayer = player
    var scoreFinal = scoreFinal
}