package com.erwan.spacedim.views.score

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.erwan.spacedim.API.User
import com.erwan.spacedim.R
import com.erwan.spacedim.databinding.ScoreFragmentBinding

class ScoreFragment : Fragment() {

    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreFragmentModelFactory
    private val TAG = "LobbyFragment"
    private lateinit var binding: ScoreFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = ScoreFragmentBinding.inflate(inflater,container,false)
        viewModelFactory = ScoreFragmentModelFactory(
            ScoreFragmentArgs.fromBundle(requireArguments()).user
        )

        viewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)
        viewModel.currentPlayer

        val gameScoreObserver = Observer<List<User>> { newState ->
            // binding of viewModel.userScoreList, draw the score on the screen foreach score
            if (newState != null) {
                Log.i(TAG, newState.toString())
                drawUserScore(newState)
            }
        }

        viewModel.userScoreList.observe(viewLifecycleOwner, gameScoreObserver)
        binding.buttonReplayScoreboard.setOnClickListener {
            val action = ScoreFragmentDirections.actionScoreDestinationToLobbyDestination (viewModel.currentPlayer)
            NavHostFragment.findNavController(this).navigate(action)
        }
        return binding.root
    }

    // create a score container (pseudo : score) for a player
    private fun createPlayerScoreContainer(user: User) : ConstraintLayout {
        val inflater = LayoutInflater.from(this.context)
        val playertile = inflater.inflate(
            R.layout.score_container,
            null,
            false
        ) as ConstraintLayout
        val name= playertile.findViewById<TextView>(R.id.text_username)
        val score= playertile.findViewById<TextView>(R.id.text_user_score)
        name.text = user.name
        score.text = user.score.toString()
        return playertile
    }

    // draw all scores in listScore
    private fun drawUserScore(users : List<User>){
        binding.listScore.removeAllViews()
        for (user in users){
            binding.listScore.addView(createPlayerScoreContainer(user))
        }
    }
}