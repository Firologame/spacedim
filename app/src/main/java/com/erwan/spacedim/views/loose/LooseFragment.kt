package com.erwan.spacedim.views.loose

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.erwan.spacedim.databinding.LooseFragmentBinding

class LooseFragment : Fragment() {


    private lateinit var binding: LooseFragmentBinding
    private lateinit var viewModel: LooseViewModel
    private lateinit var viewModelFactory: LooseViewModelFactory


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewModelFactory = LooseViewModelFactory(
            LooseFragmentArgs.fromBundle(requireArguments()).user,
            LooseFragmentArgs.fromBundle(requireArguments()).scoreFinal
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(LooseViewModel::class.java)
        binding = LooseFragmentBinding.inflate(inflater,container,false)

        // when loose view load, set the score to the final score
        binding.textLooseScore.text = viewModel.scoreFinal.toString()

        // when click on replay, navigate to lobby fragment
        binding.buttonReplay.setOnClickListener {
            val action = LooseFragmentDirections.actionLooseDestinationToLobbyDestination(viewModel.myPlayer)
            NavHostFragment.findNavController(this).navigate(action)
        }

        // when click on leaderboard, navigate to score fragment
        binding.buttonHighscore.setOnClickListener{
            val action = LooseFragmentDirections.actionLooseDestinationToScoreDestination(viewModel.myPlayer)
            NavHostFragment.findNavController(this).navigate(action)
        }
        return binding.root

    }
}