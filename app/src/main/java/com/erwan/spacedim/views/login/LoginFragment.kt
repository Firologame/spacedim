package com.erwan.spacedim.views.login

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.erwan.spacedim.databinding.LoginFragmentBinding

class LoginFragment : Fragment() {


    private lateinit var binding: LoginFragmentBinding
    private lateinit var viewModel: LoginViewModel

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate view and obtain an instance of the binding class -> https://developer.android.com/topic/libraries/view-binding
        binding = LoginFragmentBinding.inflate(inflater,container,false)

        binding.lifecycleOwner = this

        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        viewModel.userFromAPI.observe(viewLifecycleOwner, Observer {
            val action = LoginFragmentDirections.actionLoginDestinationToLobbyDestination(viewModel.userFromAPI.value!!)
            NavHostFragment.findNavController(this).navigate(action)
        })

        binding.buttonStart.setOnClickListener {
            // when click on start button, check if input is empty, if not, use retrofit to request to the api the user informations
            var pseudo = binding.inputUsername.text.toString()
            if(pseudo.length !== 0) {
                viewModel.findUser(pseudo)
            }
        }
        return binding.root
    }
}