package com.erwan.spacedim.views.win

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.erwan.spacedim.databinding.WinFragmentBinding

class WinFragment : Fragment() {

    private lateinit var binding: WinFragmentBinding
    private lateinit var viewModel: WinViewModel
    private lateinit var viewModelFactory: WinViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModelFactory = WinViewModelFactory(
            WinFragmentArgs.fromBundle(requireArguments()).user,
            WinFragmentArgs.fromBundle(requireArguments()).scoreFinal
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(WinViewModel::class.java)
        binding = WinFragmentBinding.inflate(inflater,container,false)
        binding.textWinScore.text = viewModel.scoreFinal.toString()

        // when click on replay button, navigate to lobby fragment
        binding.buttonPlayAgain.setOnClickListener {
            val action = WinFragmentDirections.actionWinDestinationToLobbyDestination(viewModel.myPlayer)
            NavHostFragment.findNavController(this).navigate(action)
        }

        // when click on leaderboard button, navigate to score fragment
        binding.buttonLeaderboard.setOnClickListener{
            val action = WinFragmentDirections.actionWinDestinationToScoreDestination (viewModel.myPlayer)
            NavHostFragment.findNavController(this).navigate(action)
        }
        return binding.root
    }
}