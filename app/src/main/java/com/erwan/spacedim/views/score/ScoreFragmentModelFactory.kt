package com.erwan.spacedim.views.score

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erwan.spacedim.API.User

class ScoreFragmentModelFactory(private val player: User) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(player) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}